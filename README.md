# GIFBOX-t

The GIFBOX project : Make GIF, Have Fun, Share It.

**Disclaimer**: it is a side project of a volounteering activities in an association called [Ubuntu-fr](https://ubuntu-fr.org) during [UbuCon Europe](https://ubucon.eu) or [Ubuntu Webcafe](https://webcafe-ubuntu.org): only few time is spend on this project because many other thing are needed to be done, every single time has been spend on best effort, it works, but, everything should be better "if I had the time" ^^ *(and you are welcome to contribute ofc!)*

### Menu
 * [Installation](#installation)
     * [Hardware](#Hardware)
     * [Software](#Software)
 * [Overlay (Choose/create your theme)](#overlay-theme-of-the-gif)
 * [LEDs (8x8 custom animation)](#leds-animation)
 * [WEB (Optionnal)](#Web)
 * [TODOs](#customization-todos)

### ![Webcafe Ubuntu](https://webcafe-ubuntu.org/wp-content/uploads/2015/07/logo-webcafe-ubuntu1.png)

You can see how the GIFBOX is used in an Ubuntu Booth during a music festival in this **[blogpost](https://webcafe-ubuntu.org/p2n/la-boite-a-gifs-toute-une-histoire/)** (there is short a video showing the GifBox, the light we used, and a screen witht the TV layout)

The first time we used the box was in 2016 as you can see on this **[blogpost](https://webcafe-ubuntu.org/webcafe/wow-samedi-vc2016/)** and you can see a video *(in the biggest music festival in France)* of the booth, and you will obviously spot the 1st Gifbox we made **[here](https://webcafe-ubuntu.org/webcafe/le-webcafe-ubuntu-aux-vieilles-charrues-2016-la-video/)**

| The new gifbox (video) | The old gifbox | The old (video) |
|---|---|---|
| [![The new GifBox](https://webcafe-ubuntu.org/wp-content/uploads/2022/06/photo5827990919973026653.jpg)](https://webcafe-ubuntu.org/p2n/la-boite-a-gifs-toute-une-histoire/) | [![The old gibox](https://webcafe-ubuntu.org/wp-content/uploads/2016/07/ufr-wow.png)](https://webcafe-ubuntu.org/webcafe/wow-samedi-vc2016/) | [![The video](https://webcafe-ubuntu.org/wp-content/uploads/2016/09/webcafeubuntu-1024x640.png)](https://webcafe-ubuntu.org/webcafe/le-webcafe-ubuntu-aux-vieilles-charrues-2016-la-video/) |


## Installation
  
### Hardware

 * Raspberry 
    * Raspberry PI 3 or 4 (with WiFi if you want hotspot, or plug it to a WiFi roubter and/or use another raspi/pc to exposed a WiFi)  
        * *(connected to a power source, can be a power bank)*
        * (we used a 3b+)
    * SD cards (we used a 16 GB class10)
 * Display: 
    * *[Current implementation]* 8x8 LED display plugged to a max7219 ([See picture](/doc/help_with_wires/LEDs_matrice_8x8.jpg))
    * *[Optionnal]* display index-tv.html in full screen (better if it not directly plugged to the gifbox, but on another pc/raspi)
 * Trigger:
    * Any button you like, by default plugged to GPIO_26 (and ground). You can update this in config.py.
 * Cam:
    * PiCam (didn't tryed with a webcam)
 * Wires:
    * To connect the 8x8 LED and the button (with PINs)
  
 * Don't forget :
    * A box (we used a electic wire box paint/stickered)
    * Feet (We used a cheap tripod and fixed the box on top of it)
    * Light (We have booths light + a decathlon camping light attached to the tripod so you face always has light)
    * Share a pic of your own box, will publish on the gitlab project ;)
  
  
    * **You can find pictures of all the things connected together in the folder [/doc/help_with_wires/](/doc/help_with_wires/)**
        * Note that the button has a red & black wires, but is connected to a white & black (white is connected to red) in case you didn't see from where it came
        * Note that the 8x8 LED are plugged to a rainbow wires, but only a few of those wires are indeed used (it was just more, beautiful?) : black, white, grey, purple and blue
        * We used a raspi 3b+, if you use the raspi 4 (or newer?), read the documentation on how to plug a single max7219, and if you change the GPIO of the button, update the GPIO number in the `config.py` (default is gpio_button=26). You can check online for GPIO number on raspi, or by using on your raspi this command: `gpio readall` (CLI)
        * Better connect things when raspi is switched off, so you can double check on a schema that you didnt plugged power to something that could break your device for eg !
        * *Disclaimer: your device, your manipulations, your mistake (my pics are just here to help you, still double check on official raspi documentation or `gpio readall`).*

| The Box | Inside overview |
|---|---|
| ![The Box](/doc/help_with_wires/the_box.jpg) | ![Inside overview](/doc/help_with_wires/pi_gpio_0.jpg) |


### Software

 * We used a *Raspbian* that doesnt `startx` automaticly (used whatever you are confortable with, but, don't start X for ressources)
    * You could probably use any other OS that boot on your raspi
    * Install and launch the GIFBOX-t:
```bash
cd /home/pi
git clone git@gitlab.com:cm-t/gifbox-t.git gifbox
cd gifbox
./start.sh
```

 * We added `start.sh` on the boot, so it starts automaticly
    * **Important** we currently didnt have the time to add a setting to configure the size of the photo to be shoot from the picam
        * It means you should NOT plug a screen to the pi until it has booted (better use ssh if you need to change anything)
        * I personnaly added the ssh://pi@ip to nautilus to update few thing on the fly in the index.html during the event
    * If you plug a screen, for example 1400x900, the photo will be shoot in 1400x900, and not in 640 × 480 pixels
        * You will have better quality photo, but it might takes a little bit longer to generate the GIF
        * You might have overlay not scalling well *( a watermark of 640 × 480 pixels is smaller than 1400x900 )*
        * GIF are suposed to be trash quality memes, change the project to MP4 LOOP BOX if you want better quality
            * Yes, the GIFBOX already generate a MP4 (from the GIF), but it is just because some people want to publish the GIF in their (insta) story, and ofc it doesn't accept GIF…
        * *(I will definitly accept a merge request if you want to gen mp4 by defaut and optionnaly gif from the mp4)*
        * MP4 is currently downloadable only from the email, but you can easly update `index.html` and phone.js to add a button for the pm4 (copy the GIF button, and use .pm4 instead of gif)
    * dependencies to be installed:
        * `git` to clone the project :)
        * `screen` (we send the server in a screen)
        * `gm, composite, convert` (graphicsmagick)
        * `python` (3)
        * `jpegoptim` (compression)
        * `ffmpeg` (for the mp4)
        * `php` (for email)
        * I hope i didnt forget any dependencies, else, bug report/mr/pm me
   
## Overlay (Theme of the GIF)

 * You can find a folder named `watermark`
    * Inside there are many folder, create yourFolerWithoutspace (I didnt checked, but just in, use camelCase)
    * You can base your work from the `UbuConEU19` folder:
         * There are 12 requiered files: *watermark-1.png, watermark-2.png, watermark-3.png, watermark-4.png, watermark-5.png, watermark-6.png, watermark-7.png, watermark-8.png, watermark-9.png, watermark-10.png, watermark-11.png, watermark-12.png*
    * Those are the frame you will need to make yourelf (you can use my .xcf in gimp if you want)
    * the 12 file will be added to the GIF in the order of their name (watermark-1.png, then watermark-2.png, etc)
    * Find watermark inspiration from some GIF we made at https://webcafe-ubuntu.org/ !
 * Edit the `config.py` file to make sure it will use your created theme : `watermark = 'UbuConEU19/'`
 ![UbuConEU](/web/last-1.gif)


## LEDs animation

 * These are the defaults animations:

| Waiting for the button | Shooting photos | Rendering |
|---|---|---|
| ![Push the button](/doc/Waiting.gif) | ![Photos, move!](/doc/Shooting.gif) | ![Rendering, wait](/doc/Rendering.gif) |

 To customise the LEDs animation, edit `images.py` (and have fun with it!) : 
 * Animations
    * An animation is simply a list of "image" (defined by a name):
```python
def get_the_animation(name):
    animations = {
        "Waiting for buton to be pushed !": [	#	← Name of the animation (don't change the name, it is called by the name of the step from gifbox.py)
            "bouton_1",				#	← Means it will start by displaying the image named "bouton_1"
            "bouton_2",				#	← Means it will continue by to displaying the image named "bouton_2"
            "bouton_3",				#	← I think you now understand
            "bouton_4"				#	← This is the last one, NO comma in the end
        ],
         "Take Photo 1/4": [			# 	← another animation bloc
            "3",
            "2",
            "1",
            // etc (...)
            					# Add more animation if you wish, the last one with no comma after the '}'
```

 * Images
     * An image is defined by more metadata such as time, contrast and all the LEDs states:
```python
    images = {
        "bouton_0": {				#	← Name of the image, that's how it is callezd from an animation, animations are called from the gifbox.py
            "contrast": 20,			#	← Contrast, from 0 to 100
            "time": 0.1,			#	← Time, is seconde (means here is 100ms)
            "pixels": [				#	← The matrice here is almost a what you see is what you get ;)
                [0,1,1,1,1,1,1,0],		#	← 1 for LED on, 0 for LED OFF
                [0,0,1,1,1,1,0,0],
                [0,0,0,1,1,0,0,0],
                [0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0],
                [0,1,1,1,1,1,1,0],
                [0,1,1,1,1,1,1,0],
                [1,1,1,1,1,1,1,1]
            ]
        },
        "GO_2-4": {				#	← This is another kind of image that can animate itself (mostly blinking-ish settigns as you will see below)
            "contrast_animate" : {		#	← It will have only this metadata
                100:0.2,			#	← It can have as many line like this as you want
                20:0.2,				#	← it has two parameters, formated like that → 1st:2nd
                100:0.2,			#		The first number is the contrast
                20:0.2,				#		The second is the time it must keep this contrast
                100:0.2				#	← The last one does NOT have a coma
            },
            "pixels": [				#	← Same as the previous image, edit the 0 and the 1 at wish :)
                [0,1,1,0,0,0,1,0],
                [1,0,0,1,0,1,0,1],
                [1,0,0,0,0,1,0,1],
                [1,0,1,1,0,1,0,1],
                [1,0,0,1,0,1,0,1],
                [0,1,1,0,0,0,1,0],
                [0,0,0,0,0,0,0,0],
                [1,1,1,1,1,1,1,1]
            ]
        },					# Add more images, the last one with no comma after the '}'
```
 * I dont have any tools to translate a picture to 0&1, but you can find my sketch/mockup in the file [pixels.ods](/doc/pixels.ods) in case it helps *(fyi, it is not updated with the recent state of the animations)*


## Web ! (optionnal)

 * There are currently 2 screens to be display (and served by a server)
    * `index.html` (for phone who connect to WiFi)
        * responsive is limited, works in PC, but "optimized" on phone
        * if you want to hide the instructions, in `web/css/gifbox.css` near l.113, change `#block-howto{ width: 60%;` with `#block-howto{ width: 60%;opacity:0;`
    * `index-tv.html` (for the TV screen you want to expose the latest GIF so people can smile together when the GIF is ready)
        * if you don't set a kiosk mode, but just use a **fullscreen** of your browser (F11) then, **refresh** this page (CTRL+R or F5) **after** you put it on fullscreen, it will help to scale the things better, just do it, you will understand
        * It is possible it displays not very well on your screen, the zoom-in (or zoom out) (CTRL+Scroll or CTRL+/-), and **refresh** between every try
        * Best display is when the mascot will have its bottom cut a bit when animated at its most bottom position; and left border of the latest (biggest) gif is the same with the bottom margin
  
| TV `index-tv.html` | Phone `index.html` |
|---|---|
| ![TV screen](/doc/Screen-TV.png) | ![Phone screen](/doc/Screen-Phone.png) |

### TV screen

## Customization & TODOs

 * *[TODO]* We should use a dependency tools (like pip or composer, npm, w/e) or a setup.sh (since we have many language plus apt maybe?) so the project would be lighter, and cleaner, and safer (security/outdated version is a thing…)
 * *[TODO]* PiCam Resolution should be a config, not default screen size
 * *[TODO]* You can duplicate `web/index-tv.html` & `web/js/gifbox.js` to `web/index-box.html` (to display in fullscreen/kiosk mode) to a connected screen, and change the way `updateStep(msg, type= 'success')` in `gifbox.js` will display the status. You should remove all the section of the html and keep only the status section to a fullscreen card.
 * Protect injection on the send by email *(wasnt a priority due to the context people used the webcafe, but could be for bigger event where you can't check on everyone)*
 * *[TODO]* Responsive should be better...
 * *[TODO]* Update the TODOs (we should use the ticket system for that), definitly this project have many needs we already listed during event of Ubuntu-fr, but it needs to be written to make sure we do not forget
