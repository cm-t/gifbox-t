<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

//Below is the content of email, fell free to put whatever you want here (dont forget config from l36 to 
$bodytext = <<<TXT
Bonjour cher·e festivalier·e

Voici un souvenir des Papillons de Nuit 2022 au webcafé Ubuntu !

Profite bien de la fin du festival, et à bientôt :)

L'équipe du webcafé Ubuntu

-- 

Retrouve le blog du webcafé:
https://webcafe-ubuntu.org

La communauté ubuntu-fr:
https://ubuntu-fr.org

TXT;

// we receive from GET data (and also from arg if you test the script from CLI)
$courriel = array_key_exists(1, $argv) ? $argv[1] : $_GET['email']; #TODO échapper (escape properly against injection)
$gif = array_key_exists(2, $argv) ? $argv[2] : $_GET['gif']; #TODO échapper (escape properly against injection)

$email = new PHPMailer();
$email->CharSet = 'UTF-8';

$email->Body      = $bodytext;

$email->AddAddress( $courriel );

$email->SetFrom('gifbox@your-server.com', 'Ubuntu GIFBOX');
$email->Subject   = 'My GIF from Ubuntu GIFBOX';
set_time_limit(5);
$email->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
$email->isSMTP();                                            //Send using SMTP
$email->Host       = 'smtp.your-server.fr';                     //Set the SMTP server to send through
$email->SMTPAuth   = true;                                   //Enable SMTP authentication
$email->Username   = 'gifbox@your-server.fr';                     //SMTP username
$email->Password   = 'smtp_password_here';                               //SMTP password
$email->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
//$email->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
$email->Port       = 465;
$email->Timeout       =   5;

$file_to_attach = './pics/' . $gif   ;

$email->AddAttachment( $file_to_attach . '.gif', 'Mon_GIF_Ubuntu_aux_P2N22.gif' );
$email->AddAttachment( $file_to_attach . '.mp4', 'Ma_Story_Ubuntu_aux_P2N2.mp4' );
$email->AddAttachment( $file_to_attach . '.jpg', 'Miniature_Ubuntu_aux_P2N22.jpg' );

if ($email->Send()) {
	echo 'true';
}
echo 'false';
