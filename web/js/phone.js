pics = {
	"pics": [
		"2019-10-12_14-25-04.gif"
	]
}
;

thankyou = 0;
$( document ).ready(function() {
	$('#loadingdiv').hide();
	$('#loadingdiv').css('opacity', '1');
	$( "#top .nav-link" ).click(function() {
		// TODO: use class instead of ID for group action (was for dev purpose)	
		window.scrollTo(0, 0);
		$( ".nav-link" ).removeClass('active');
		$( this ).addClass('active');
		$("#bottom").hide();
		$("#thankdiv").hide();
		if(this.id == 'nav-search') {
			document.getElementById("formseach").reset();
			$("#searchdiv").html('');
			$('.timesearch').removeClass('alert-success');
			$('.datesearch').removeClass('alert-success');
			$("#sharediv").show();
			$("#search").show();
			$("#lastdiv").hide();
			$("#searchdiv").hide();
			$("#refresh").hide();
			getPicsList();
			pics = sortPics(pics);
		} else {
			$("#refresh").show();
			$("#lastdiv").hide();
			$("#searchdiv").hide();
			$("#search").hide();
			$("#refresh").hide();
			var x = setTimeout("$('#sharediv').hide();$('#lastdiv').show();$('#top').show();$('#refresh').show();$('img').unveil(1, function() {});",2000);
		}
	});
	
	$("#datesearch").on('change', function(){
		$('.datesearch').addClass('alert-success');
		launchSearch();
	});
	$("#timesearch").on('change', function(){
		$('.datesearch').addClass('alert-success');
		$('.timesearch').addClass('alert-success');
		launchSearch();
	});
	$("#refresh").on('click', function(){
		$("#sharediv").show();
		$("#bottom").hide();
		$("#thankdiv").hide();
		$("#top").hide();
		$("#lastdiv").hide();
		location.reload(true)
	});
	$(".ablock img").on('click', function(){
		$(this).attr('src', 'pics/' + $(this).closest('.ablock').attr('id'));
	});
	$(".btn-preview").on('click', function(){
		$(this).closest('.ablock').find('img').attr('src', 'pics/' + $(this).closest('.ablock').attr('id'));
	});
	$('#top').hide();

	var x = setTimeout("$('#sharediv').hide();$('#lastdiv').show();$('#top').show();$('#thankdiv').hide();$('#refresh').show();$('img').unveil(1, function() {});",3000);
	$("#sharediv").on('click', function(){
		thankyou = thankyou + 1;
		if(thankyou%3==0){
			$("#sharediv").toggle();
			$("#thankdiv").toggle();
		}
	});
	//getPics(getPic(getsinceString('2019-09-09','10:10'), 25),25);

});

function launchSearch(id = false){
	window.scrollTo(0, 0);
	if(id === false) {
		var date = $("#datesearch").val();
		var time = $("#timesearch").val();
	} else {
		var dt = getPicDateTime(id);
		var date = dt.date;
		var time = dt.time;
	}
	// TODO CONTROL DATE AND TIME ... REALLY
	$("#thankdiv").hide();
	$("#sharediv").show();
	$("#searchdiv").hide();
	$('#bottom').hide();
	var piclist = getPics(
		getPic(
			getsinceString( date, time ),
			25
		),
		25
	);
	$("#timesearch").val(getPicDateTime(piclist.start).time);
	$("#timesearchend").val(getPicDateTime(piclist.end).time);
	$("#searchdiv").html(getResultDOM(piclist));
	var x = setTimeout("$('.timesearch').removeClass('alert-success');$('.datesearch').removeClass('alert-success');",2000);
	$('#searchdiv').show();
	$('#sharediv').hide();
	$("img").unveil(1, function() {
		//console.log('laoded');
	});
	$(".ablock img").on('click', function(){
		$(this).attr('src', 'pics/' + $(this).closest('.ablock').attr('id'));
	});
	$(".btn-preview").on('click', function(){
		$(this).closest('.ablock').find('img').attr('src', 'pics/' + $(this).closest('.ablock').attr('id'));
	});

	if(piclist.prev == false){
		$("#nav-prev").addClass('disabled');
	} else {
		$("#nav-prev").removeClass('disabled');
		if(piclist.start <= 25){
			var startp = 0;
		} else {
			var startp = (piclist.start - 25);
		}
	}

	if(piclist.next == false){
		$("#nav-next").addClass('disabled');
	} else {
		$("#nav-next").removeClass('disabled');
		if(piclist.end > (pics.pics.length -25) ){
			var startn = (pics.pics.length -25);
		} else {
			var startn = piclist.start + 25;
		}
	}

	$("#nav-prev").attr('onclick', "launchSearch(" + startp + ");");
	$("#nav-next").attr('onclick', "launchSearch(" + startn + ");");
	$('#bottom').show();
}

/**
 * get display name for a title of image
 */
function getPicName(key){
	var pic = pics.pics[key];
	pictmp = pic.split("_");
	return pictmp[0] + '  ' + pictmp[1].replace('-', ':').replace('-', ':').replace('.gif', '');
}

/**
 * get date and time
 * @param {} key 
 */
function getPicDateTime(key){
	var pic = pics.pics[key];
	var pictmp = pic.split("_");
	var pictmp2 = pictmp[1].replace('-', ':').split("-");
	return {"date": pictmp[0], "time": pictmp2[0]};
}

/**
 * Sort gif in order recent>older 
 * @param {object} pipics with pics array
 */
function sortPics(pipics){
	pipics.pics.sort(function (a, b) {
		return b.localeCompare(a);// invert so recent at first
	});
	return pipics;
}

/**
 * Fetch a recent list of the pics
 */
function getPicsList(){
	$.get( "pics.json" + '?t=' + $.now(), function( data ) {
		pics = sortPics(data);
	}).done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "Failed to load pics list.")
		// var msg = "Error while refreshing state; check if the GIFbox is started?"
		// updateStep(msg, 'danger');
	})
	.always(function() {
		// $('#loading').hide();
		console.log( "End query.");
		// var x = setTimeout("getGifBoxStatus()", next[step]);
	});
}

function getResultDOM(picsFound){
	var res = '';
	if(picsFound.error) {
		res = '<span class="alert-danger"> No result found. </span>';
		return res;
	}
	for (var i = picsFound.start; i < picsFound.end; i++) {
		res = res + getPicDOM(i);
	}
	return res;
}

function getPicDOM(picId){
	var pipics = pics.pics;
	var gif = pipics[picId];
	var jpg = gif.replace('gif', 'jpg');
	var mp4 = gif.replace('gif', 'mp4');
	var titre = gif.replace('gif', 'jpg');
	var attachement = gif.replace('.gif', '');
	
	var dom = ' ' +
	'<div class="ablock" id="' + gif + '">' +
	'	<img data-src="pics/' + jpg + '" src="img/loading.gif">' +
	'	<div class="tagging">' +
	'		<span class="badge badge-pill badge-primary">@UbuntuFrOrg</span>' +
	'		<span class="badge badge-pill badge-secondary">#UbuConEurope</span>' +
	'		<p> 📆 ' + getPicName(picId) + '</p>' +
	'	</div>' +
	'	<div class="btns-search float-right">' +
	'		<a type="button" class="btn btn-labeled btn-info btn-preview" href="#' + gif + '">' +
	'			<span class="btn-label">' +
	'				<i class="fa fa-eye" aria-hidden="true"></i>' +
	'			</span>' +
	'		Preview</a>' +
	'		<a type="button" class="btn btn-labeled btn-info"  href="pics/' + gif + '" download="MyGIF_at_UbuConEurope.gif">' +
	'			<span class="btn-label">' +
	'				<i class="fa fa-download" aria-hidden="true"></i>' +
	'			</span>' +
	'		Download</a>' +
	'		<a type="button" class="btn btn-labeled btn-success btn-block" onclick="prepareSend(\'' + attachement + '\');" href="#' + gif + '">' +
	'			<span class="btn-label text-left">' +
	'			<i class="fas fa-envelope" aria-hidden="true"></i>' +
	'</span>' +
	' Send it to my Email</a>' +
	'	</div>' +
	'</div>';
	return dom;

}

function prepareSend(attachement) {
	let mel = prompt("Insert your email here !", "my.email@example.com");
	$('#loadingdiv').show();
	$('#top').hide();
	$("#lastdiv").css('opacity', '0');
	$("#searchdiv").css('opacity', '0');
	if (mel != null && sendMel(mel, attachement)) {
		$('#loadingdiv').show();	
		$('#top').show();
		$("#lastdiv").css('opacity', '0');
		$("#searchdiv").css('opacity', '0');
	} else {
		$('#loadingdiv').hide();	
		$('#top').show();
		$("#lastdiv").css('opacity', '1');
		$("#searchdiv").css('opacity', '1');
		//alert('⚠️ Erreur lors de l\'envoi, vérifie que tu as correctement renseigné ton adresse !');
	}
}

function sendMel(email, attachement) {
	var retrn = false;
	$.ajax({
        url: 'sendGif.php?gif=' + attachement + '&email=' + email,
        success: function (result) {
		console.log(result);
		if (result == 'false') {
        		alert('⚠️ something went worng, please check if you correctly typed your email adress !');
		} else {
			alert('😎 your GIF has being sent !\n\n🎉 Enjoy the event <3');
			
		}
	},
        async: false
    });
	return retrn;
}

/**
 * Get the searched images to display
 * @param {int} sinceId key of the gif in pics
 * @param {int} gap amount of pics to fetch (+ or -)
 * @return TODO
 */
function getPics(sinceId, gap){

	var pipics = pics.pics;
	var size = pipics.length - 1;
	var tilId = sinceId + gap;
	var canNext = true;
	var canPrev = true;
	if(sinceId == -1){
		sinceId = size - (Math.abs(gap));
		tilId = size;
	}
	if(tilId >= size) {
		tilId = size;
		canNext = false;
	}
	if(tilId < 0) {
		var tmpId = tilId;
		tilId = sinceId;
		sinceId = tmpId;
	}
	if(sinceId == 0) {
		canPrev = false;
	}
	return {
		"error" : false,
		"start": sinceId,
		"end": tilId,
		"next" : canNext,
		"prev" : canPrev,
	};
}

/**
 * get a string "filename" to be fetchable in pics
 * @param {string} theDate date to search
 * @param {string} theTime time to search
 */
function getsinceString(theDate, theTime) {
	return theDate + "_" + theTime.replace(':', '-') + "-00.gif";
}

/**
 * fetch the closest pic to the the fetchable string 
 * @param {int} sinceString 
 * @param {int} gap amount of pics to fetch (+ or -) 
 */
function getPic(sinceString, gap) {
	var pipics = pics.pics;
	var index = pipics.indexOf(sinceString);
	if( index >= 0 ){ // juste in case it perfectly exist already ;)
		return index;
	}
	var sens = (gap > 0 ? 1 : -1);
	var ret = -1;
	pipics.forEach(function(element) {
		if( element.localeCompare(sinceString) != sens  && ret == -1){
			ret = pipics.indexOf(element);
		}
	});
	ret = ret - 1;
	if (ret < 0) {
		ret = 0;
	}
	return ret;
}
