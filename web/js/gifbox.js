
// configs
total = -1;
step = 'Init';
next = {
	"Waiting for buton to be pushed !" : 5000,
	"Take Photo 1/4" : 2000,
	"Take Photo 2/4" : 2000,
	"Take Photo 3/4" : 2000,
	"Take Photo 4/4" : 2000,
	"Rendering..." : 9000
};

// customise status sentence here
enToMine = {
	//"key (don't change)" : "Value (customise here)",
	"Init" : "Initialisation",
	"Waiting for buton to be pushed !" : "Push the button&nbsp;!",
	"Take Photo 1/4" : "Shooting session, smile! (1/4)",
	"Take Photo 2/4" : "Shooting session, turn around! (2/4)",
	"Take Photo 3/4" : "Shooting session, Jump around! (3/4)",
	"Take Photo 4/4" : "Shooting session, Put your hands up in the air! (4/4)",
	"Rendering..." : "Your GIF is rendering…",
	"Error while refreshing state; check if the GIFbox is started?" : "Error while fetching GIFBOX status, is it started&nbsp;?"
};

// french is already done here
enTofr = {
	"Init" : "Initialisation",
	"Waiting for buton to be pushed !" : "Appuyer sur le bouton&nbsp;!",
	"Take Photo 1/4" : "GIF en cours (Photo 1 sur 4)",
	"Take Photo 2/4" : "GIF en cours (Photo 2 sur 4)",
	"Take Photo 3/4" : "GIF en cours (Photo 3 sur 4)",
	"Take Photo 4/4" : "GIF en cours (Photo 4 sur 4)",
	"Rendering..." : "Génération du GIF en cours…",
	"Rendering..." : "Génération du GIF en cours…",
	"Error while refreshing state; check if the GIFbox is started?" : "Erreur lors de l'obtention de l'état de la boîte à GIF, est-elle démarrée&nbsp;?"
};
// Start 1st check after 5sec
var x = setTimeout("getGifBoxStatus()", 5000);

//Call from the GIFbox, its status
function getGifBoxStatus(){
	$('#loading').show();
	$.get( "status.json" + '?t=' + $.now(), function( data ) {
		updateStatus(data);
	}).done(function() {
		console.log( "success" );
	})
	.fail(function() {
		var msg = "Error while refreshing state; check if the GIFbox is started?"
		updateStep(msg, 'danger');
	})
	.always(function() {
		$('#loading').hide();
		console.log( "Next check in " + next[step] + "ms.");
		var x = setTimeout("getGifBoxStatus()", next[step]);
	});
}

// Are status changed?
function updateStatus(gifboxStatus){
	if (total != gifboxStatus.total) {
		total = gifboxStatus.total;
		updateTotal();
	}
	if (step != gifboxStatus.step) {
		step = gifboxStatus.step;
		updateStep(step);
	}
}

//reload the images without reloading the page
function reloadPics(){
	$(".img").each(function() {
		var imgsrc = $(this).attr('src');
		imgsrc = getUrlNoGET(imgsrc);
		$(this).attr("src", imgsrc + '?t=' + $.now());
	});
}

// Get an url without GET param
function getUrlNoGET(str) {
	var i = str.indexOf("?");
	if(i > 0)
		return  str.slice(0, i);
	else
		return str;     
}

// update the total, and trigger reload image
function updateTotal(){
	$('#total').html(total);
	reloadPics();
}

// update the status
function updateStep(msg, type= 'success'){
	$('#step').html('<span class="alert-' + type + ' text-' + type + '">' + tanslate(msg) + '</span>');
}

function tanslate(msg){
	return (msg in enToMine ? enToMine[msg] : msg);
}

