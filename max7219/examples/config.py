#GPIO configs
gpio_button=26 # GPM ID (cf 'gpio readall')

#Matrice 8x8 configs
matrice_rotation=3 # make sure the display of your 8x8 matrix LED is well rotated

#Config settings to change behavior of photo booth
total_pics = 4 # don't change this yet, not sure everything works if not 4, yet?'
gif_delay = 20

clear_on_startup = False # True will clear previously stored photos as the program launches. False will leave all previous photos.
clear_jpg = True #Every time we build a gif, we remove all jpg, keep only gif
self_host = True #Should we host on LAN ? (eg: display a connect to WiFi to download your pic)

file_path = '/home/pi/gifbox/web/pics/' # path to save images (ending with a `/`)
web_path = '/home/pi/gifbox/web/' # path to publish images (ending with a `/`)
web_port = 8000 # http port url

## python server
#host_start = 'screen -S gifboxserver -d -m bash -c "cd {web_path}; python -m SimpleHTTPServer {web_port}"'

##php server
host_start = 'screen -S gifboxserver -d -m bash -c "cd {web_path}; php -S {web_ip}:{web_port}"'

## kill server php/python
host_quit = 'screen -S gifboxserver -X quit'
web_ip = True # If not True, set it manually (eg: 192.168.0.4)
