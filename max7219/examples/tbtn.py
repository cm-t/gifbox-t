import config

from gpiozero import Button
from picamera import PiCamera
from datetime import datetime
from signal import pause

#init hardware <--> lib
button = Button(config.gpio_button)
camera = PiCamera()

def btn_pressed():
    print('pressed !!!')

print('listening button GPIO_' + str(config.gpio_button))
button.when_pressed = btn_pressed

pause()

