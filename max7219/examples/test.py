import time
import re
import argparse
import subprocess
import os
import glob

#if self_host

#Custom config & image import
from images import get_image, get_the_animation
import config

# import for button & cam
from gpiozero import Button
from picamera import PiCamera
from datetime import datetime
from signal import pause

# import fot 8x8 matrix
from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.core.virtual import viewport
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, CP437_FONT, TINY_FONT, SINCLAIR_FONT, LCD_FONT

# Reminder ;)
print('Checklist:')
print(' [X] picam enabled via raspi-config')
print(' [X] LCPI enabled via raspi-config')
print(' [X] config.py is updated')
print(' [X] Listening button GPIO_' + str(config.gpio_button))
print(' [X] chmod +w / chown / groupadd in pics/gif paths ')
print('Press [CTRL]+[C] to quit')

#init hardware <--> lib
button = Button(config.gpio_button)
camera = PiCamera()

serial = spi(port=0, device=0, gpio=noop())
device = max7219(serial, cascaded=1, block_orientation=0,
                     rotate=config.matrice_rotation, blocks_arranged_in_reverse_order=0)

virtual = viewport(device, width=200, height=100)

#print an image to the 8x8 matrix
def print_image(image, device, invert = False):
    with canvas(device) as draw:
        for x in range(8):
            for y in range(8):
                #print(str(x) + ' ' + str(y))
                if (image['pixels'][x][y] ==1 and invert==False or image['pixels'][x][y] !=1 and invert==True):
                    draw.point((y,x), fill="white")
                else:
                    draw.point((y,x), fill="black")
    if "contrast_animate" in image:
        for contrast, timing in image["contrast_animate"].items():
            device.contrast(contrast)
            time.sleep(timing)
    else:
        device.contrast(image['contrast'])
        time.sleep(image['time'])

# print a message to the log + 8x8 matrix
def print_message(message, device):
    print(message)
    show_message(device, message, fill="white", font=proportional(CP437_FONT))

# Display an animation of images to the 8x8 LED matrix
def print_animation(messages, device):
    global WAITING
    lastWait = WAITING
    for message in messages:
        if (lastWait != WAITING): #There is no situation an animation should continu if the state changes
            break
        msg = message.replace('_inverted', '')
        print_image(get_image(msg), device, (msg!=message) )

# Call get_the_animation but log the step
def get_animation(name, printIt = True):
    global LASTSTEP
    if (printIt and LASTSTEP != name):
        print("Step: " + str(name))
        if(config.self_host):
            web_log(name)
    LASTSTEP = name
    return get_the_animation(name)

def web_log(step):
    if(step == "Done !" or step == "Init"):
        step = "Waiting for buton to be pushed !"
    gifs = len(glob.glob1(config.file_path,"*.gif"))
    with open(config.web_path + "status.json", "w") as text_file:
        print('{\n\t"step": "' + step + '",\n\t"total": ' + str(gifs) + '\n}', file=text_file)

#making a gif with pics, using CLI
def build_gif(now):
    for x in range(1, 13):
        y = x%4
        if y <= 0:
            y = 4
        if x<10:
            xx = "0" + x.__str__() 
        else:
            xx = x.__str__()
        yy = "0" + y.__str__() 
        #print "step_Y:" + y.__str__() + " step_X:" + x.__str__() + " step_YY:" + yy + " step_XX:" + xx
        if x < 5:
            bashCommand = "convert -flop " + config.file_path + now + "-" + yy + ".jpg " + config.file_path + now + "-" + yy + ".jpg && sleep 0.1"
            output = subprocess.check_output(['bash','-c', bashCommand])
        bashCommand = "composite -dissolve 100% -gravity south ./watermark/watermark-" + x.__str__() + ".png "+ config.file_path + now + "-" + yy + ".jpg ./frame-" + xx + ".jpg"
        #print(bashCommand)
        #os.system(bashCommand)
        output = subprocess.check_output(['bash','-c', bashCommand])
    graphicsmagick = "gm convert -delay " + str(config.gif_delay) + " ./frame-*.jpg " + config.file_path + now + ".gif" 
#    print(graphicsmagick)
    time.sleep(1)
    os.system(graphicsmagick) #make the .gif
    mv_0 = "mv " + config.web_path + "last-4.gif " + config.web_path + "last-5.gif"
    time.sleep(1)
    os.system(mv_0)
    mv_1 = "mv " + config.web_path + "last-3.gif " + config.web_path + "last-4.gif"
    os.system(mv_1)
    mv_2 = "mv " + config.web_path + "last-2.gif " + config.web_path + "last-3.gif"
    os.system(mv_2)
    mv_3 = "mv " + config.web_path + "last-1.gif " + config.web_path + "last-2.gif"
    os.system(mv_3)
    mv_4 = "cp " + config.file_path + now + ".gif " + config.web_path + "last-1.gif"
    os.system(mv_4)

# delete files
def reset_folder(maskfile=""):
    files = glob.glob(config.file_path + "*" + maskfile)
    for f in files:
        os.remove(f) 
        
def server_cmd(command = ''):
    command = command.replace("{web_path}", config.web_path)
    command = command.replace("{web_port}", str(config.web_port))
    command = command.replace("{web_ip}", str(MYIP))
    #print(command)
    os.system(command)

# if config.wep_ip is true we (try to) calculate it, else we just use the given ip.
# Not needed for python server, just php server to be eposed to LAN
def get_ip():
    if(config.web_ip == True):
        getIp =  subprocess.Popen("hostname -I | cut -f1 -d' '", shell=True, stdout=subprocess.PIPE).stdout
        myIp = str(getIp.read())
        myIp = myIp.replace('\\', '')
        myIp = myIp.replace("'", '')
        myIp = myIp.replace('n', '')
        myIp = myIp.replace('b', '')
        return myIp
    return config.web_ip

# Processing action/display when button pressed
def btn_pressed():
    global WAITING
    WAITING = False
    timestamp = time.strftime("%Y-%m-%d_%H:%M:%S")
    for i in range(1, 1+config.total_pics):#loop for pictures to be shoot'd'
        print_animation(get_animation("Take Photo " + str(i) + "/4"), device)
        camera.capture(config.file_path + '%s-0%s.jpg' % (timestamp, i))
    print_animation(get_animation("Rendering..."), device)
    build_gif(timestamp)
    if(config.clear_jpg):
        reset_folder('.jpg')
    print_animation(get_animation("Done !"), device)
    WAITING = True

# Glabal state
WAITING = True
LASTSTEP = "Init"
MYIP = get_ip()

### Begining
try:
    #Launch Web server if config says so
    if(config.self_host):
        print('Starting server: ' + str(MYIP) + ':' + str(config.web_port))
        server_cmd(config.host_start)
    #Delete JPG if config says so
    if(config.clear_on_startup):
        reset_folder()
    #Bring the thing ON!
    print_message("Start!", device)
    #Event of the button
    button.when_pressed = btn_pressed
    while True:#The display loop (Button or loading)
        if (WAITING):
            print_animation(get_animation("Waiting for buton to be pushed !"), device)
        elif(LASTSTEP=="Rendering..."):#The only reason to display something else is the loading @ rendering
            print_animation(get_the_animation("Loading..."), device)
finally:
    if(config.self_host):
        server_cmd(config.host_quit)
        print('Stopping server: ' + str(MYIP) + ':' + str(config.web_port))
    print("Quiting while step was: " + LASTSTEP)
    print_message("bye", device)






